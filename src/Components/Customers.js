import React from 'react'

export class Customers extends React.Component{
    constructor(props){
        super(props)

        this.state = {customers: [], isLoaded:false, error:null }

    }
// To POST data to the customers table

    createCustomerRestApi(){
        let customerURL = `http://localhost:8080/api/customers` 
        fetch(customerURL, {
            //Adding method type
            method: "POST",
            body: JSON.stringify({
                firstName: "Chris",
                lastName: "Paul",
                city: "Phoenix",
                zipCode: "85210",
                dateJoined: "2005-12-30"
            }),
            headers:{
                "Content-Type": "application/hal+json"
            }
        })

        .then(response => response.json())
        .then(
            json=>console.log(json)
        );
    }



    // To GET data from the customers table
    callRestApi(){

        let customerURL = `http://localhost:8080/api/customers`
        // use the ES6 fetch syntax
        fetch(customerURL)
        .then(res => res.json()) // always do this for JSON data
        // .then(map => console.log())  // then log it out
        .then(
            //To handle the result
            (result)=>{
                console.log(result)
                //Take the returned 'result' array and use it in our state
                
                this.setState( {isLoaded:true, customers:result['_embedded']['customers']} )
            },
            //To handle the error
            (error)=>{
                this.setState({isLoaded:true, error:error})
            }

        )
    }

    componentDidMount(){
        this.callRestApi()
        this.createCustomerRestApi()

    }

    render(){
        if (this.state.error){
            return(
                <aside>
                    <h4>Error: {this.state.error.message}</h4>
                </aside>
            )
        }else if (!this.state.isLoaded){
            return(
            <aside>
                <h4>Waiting...</h4>
            </aside>
            )
        }else{
            return(
                <section>
                    {/* We can explore the loaded data during development */}
                    {/* <pre>{this.state.users[1].name.toString()}</pre> */}
                    {/* We can map to iterate over the returned data */}
                    {/* <button id = "customerBtn"> Click here to create a customer!</button> */}
                    <div id = "topNav">
                        <img id = "logoImg" src="Allstate-Logo.wine.png" alt = "Allstate logo"></img>
                        <h2>Allstate customers</h2>
                    </div>
                    <button id = "createBtn" > Create customer</button>
                    <button id = "updateBtn">Update customer</button>

                    <ul>
                        {/* ES6 arrays have a 'map' operator */}
                        {this.state.customers.map((customer,i) => (
                            <table>
                            
                            <li key={i}>First Name: {customer.firstName}<br></br>
                            Last Name: {customer.lastName}<br></br>
                            City: {customer.city} <br></br>
                            Zip Code: {customer.zipCode} <br></br>
                            Date Joined: {customer.dateJoined}<hr></hr></li>
                            </table>
                        ) )}
                    </ul>


                </section>
            )
    }


}
}