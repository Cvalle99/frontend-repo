import logo from './logo.svg';
import './App.css';
import {Customers} from './Components/Customers'

function App() {
  return (
    <div className="App">
      <Customers/>
    </div>
  );
}

export default App;
